package com.newsapp.borislav.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.newsapp.borislav.data.model.News
import com.newsapp.borislav.repository.MainRepository
import com.newsapp.borislav.utils.Event

class DetailsViewModel(private val mainRepository: MainRepository) : ViewModel() {

    private val _newsId = MutableLiveData<String>()

    private val _product = _newsId.switchMap { taskId ->
        mainRepository.observeNewsById(taskId)
    }
    val product: LiveData<News> = _product

    private val dataLoading = MutableLiveData<Boolean>()
    private val _snackbarText = MutableLiveData<Event<String>>()
    val snackbarText: LiveData<Event<String>> = _snackbarText

    fun start(productId: String?) {
        // If we're already loading or already loaded, return (might be a config change)
        if (dataLoading.value == true || productId == _newsId.value) {
            return
        }
        dataLoading.value = true
        // Trigger the load
        _newsId.value = productId!!
    }
}