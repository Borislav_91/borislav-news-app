package com.newsapp.borislav.di

import com.newsapp.borislav.data.database.getDatabase
import com.newsapp.borislav.data.database.provideNewsDao
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module


val DatabaseModule = module {
    single {
        getDatabase(androidApplication())
    }
    single {
        provideNewsDao(get())
    }
}